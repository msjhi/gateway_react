package sn.mara;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("sn.mara");

        noClasses()
            .that()
                .resideInAnyPackage("sn.mara.service..")
            .or()
                .resideInAnyPackage("sn.mara.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..sn.mara.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
